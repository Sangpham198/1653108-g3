#pragma once
#ifndef Polymial_h
#define Polymial_h
#include <iostream>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream>
using namespace std;
#define Min 0.000001
typedef long long int ll;
const ll BASE = 10e8 + 3;
const int maxn = 10e6 + 1;
//declare monomial structure
struct monomial
{
	int sobien ;
	float coe;
	char *var;
	unsigned short *exp;
	monomial *pNext;
	monomial()
	{}
	monomial(monomial &a)
	{
		coe = a.coe;
		exp = a.exp;
		var = a.var;
		sobien = a.sobien;
	}
	monomial &operator=(monomial a)
	{
		monomial tmp(a);
		tmp.pNext = a.pNext;
		return tmp;
	}
	monomial operator+(monomial a)
	{
		monomial tmp;
		tmp.coe = coe + a.coe;
		tmp.exp = a.exp;
		tmp.var = a.var;
		return tmp;
	}
	monomial operator-(monomial a)
	{
		monomial tmp;
		tmp.coe = coe - a.coe;
		tmp.exp = a.exp;
		tmp.var = a.var;
		return tmp;
	}
	monomial operator*(monomial a)
	{
		monomial tmp ; // luu a 

		int m, n;
		m = a.sobien ;
		n = sobien ;

		int count = m ;

		for (int i = 0; i < n; i++)
		{
			bool test = false ; 
			for (int j = 0; j < m ; j++)
				if (var[i] == a.var[j]) // gap bien giong nhau 
				{
						test = true ; 
						break ;
				}
			if ( !test ) count++ ;
		}

		tmp.coe = a.coe * coe ;
		tmp.sobien = count;
		tmp.var = new char[count];
		tmp.exp = new unsigned short[count];

		count= 0 ; 
		for ( int i = 0 ; i < m ; i++ ) 
		{
				count++ ; 
				tmp.var[count] = a.var[i] ;
				tmp.exp[count] = a.exp[i] ;
				for ( int j = 0 ; j < n ; j++ ) 
						if ( a.var[i] == var[j] )
						{
								tmp.exp[count] += exp[j] ;
								break ; 
						}
		}

		for (int i = 0; i < n; i++)
		{
			bool test = false ; 
			for (int j = 0; j < m ; j++)
				if (var[i] == a.var[j]) // gap bien giong nhau 
				{
						test = true ; 
						break ;
				}
			if ( !test ) 
			{
					count++ ;
					tmp.var[count] = var[i] ;
					tmp.exp[count] = exp[i] ;
			}
		}

		return tmp;
	}
};

void Swap(monomial &a, monomial &b);

class Polymial
{
public:
	string loadfile(string infile);//take data from file to execute
	void storeFile(string outfile, int &f_count);//input data to file		   
	monomial *createMono(monomial data);//create a monomial node	
	void createPoly();//create a polynomial list default	
	void splitPoly(string infile);//split polynomial into monomial			  
	void enqueue(monomial data);//enqueue a monomial into polynomial		
	monomial *dequeue();//dequeue a node from polonomial					
	bool checkOperate(char ch);//check operator
	void shorten();//shorten polynomial by deleting node has coe=0
	void sortPoly();//sorting poly base on exponent and variable(in case exponents equal)
	void add2Poly(Polymial l1, Polymial l2);//add 2 Polymials	  
	void sub2Poly(Polymial l1, Polymial l2);//sub 2 Polymials		  
	void multi2Poly(Polymial l1, Polymial l2);//multiply 2 Polymial
private:
	monomial *pHead;
	monomial *pTail;
	void delete_position(int pos);
	int compare(monomial T, monomial P);
	
};
#endif