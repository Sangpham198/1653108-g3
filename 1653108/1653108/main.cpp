#include "Polymial.h"

int main()
{
	int f_count = 1;
	ofstream f_output;
	f_output.open("F-result.txt");
	f_output.close();
	Polymial p1, p2;
	p1.createPoly();
	p2.createPoly();
	string t1, t2;
	t1 = p1.loadfile("F1.txt");
	cout << "t1= " << t1 << endl;
	t2 = p2.loadfile("F2.txt");
	cout << "t2= " << t2 << endl;
	p1.splitPoly("F1.txt");
	p2.splitPoly("F2.txt");
	//cout << "Phase 1 success" << endl;
	p1.shorten();
	p2.shorten();
	//cout << "Phase 2 success" << endl;
	p1.sortPoly();
	p2.sortPoly();
	//cout << "Phase 3 success" << endl;
	p1.storeFile("F-result.txt", f_count);
	p2.storeFile("F-result.txt", f_count);
	//cout << "Phase 4 success" << endl;
	Polymial p3;
	p3.createPoly();
	p3.add2Poly(p1, p2);
	p3.storeFile("F-result.txt", f_count);
	//cout << "Phase 5 success" << endl;
	Polymial p4;
	p4.createPoly();
	p4.sub2Poly(p1, p2);
	p4.storeFile("F-result.txt", f_count);
	/*cout << "Phase 6 success" << endl;
	cout << "Success!" << endl;
	Polymial p5;
	p5.createPoly();
	p5.multi2Poly(p1, p2);
	p5.storeFile("F-result.txt", f_count);
	cout << "Phase 7 success" << endl;*/
	return 0;
}