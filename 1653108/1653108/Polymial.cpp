#include "Polymial.h"

//take data from file to execute
string Polymial::loadfile(string infile)
{
	string tmp = "";//using this (string) tmp to store data in file text;
	ifstream f_input;
	f_input.open(infile);
	if (f_input.is_open())
	{
		getline(f_input, tmp);//input polynomial from string into tmp;
	}
	f_input.close();
	return tmp;
}

string convertFloat(float a)
{
	stringstream ss;
	ss << a;
	return ss.str();
}

string convertshort(unsigned short a)
{
	stringstream ss;
	ss << a;
	return ss.str();
}
//input data to file
void Polymial::storeFile(string outfile, int &f_count)
{
	string strtmp = "";
	string value_tmp;
	ofstream f_output;
	f_output.open(outfile, ios::app);
	if (f_output.is_open())
	{
		f_output << "F" << f_count << endl;
		f_count++;
		f_output << "---";
		monomial *tmp = pHead;
		while (tmp != NULL)
		{
			value_tmp = convertFloat(tmp->coe);
			strtmp += value_tmp;
			value_tmp = convertshort(*tmp->exp);
			char *var_tmp = tmp->var;
			char var_tmp2 = *var_tmp;
			strtmp += var_tmp2;
			strtmp += "^" + value_tmp;
			if (tmp->pNext != NULL)
			{
				if (tmp->pNext->coe > 0)
				{
					strtmp += "+";
				}
			}
			tmp = tmp->pNext;
		}
	}
	cout << strtmp << endl;
	f_output << strtmp << endl;
	f_output.close();
}

//create a monomial node
monomial *Polymial::createMono(monomial data)
{
	monomial *tmp = new monomial;
	tmp->coe = data.coe;
	tmp->var = data.var;
	tmp->exp = data.exp;
	tmp->pNext = NULL;
	tmp->sobien = data.sobien;
	return tmp;
}

//create a polynomial list default
void Polymial::createPoly()
{
	pHead = pTail = NULL;
}

//split polynomial into monomial
void Polymial::splitPoly(string infile)
{
	string poly = loadfile(infile);
	poly += "+";
	monomial tmp;
	string store;

	int length = poly.length();
	for (int i = 0; i < length;i++)
	{
		store = "";
		if (poly[i] == '-')
		{
			store += poly[i];
			i++;
		}
		if (i > 0)
		{
			if (!checkOperate(poly[i]))
			{
				if (poly[i - 1] == '-')
				{
					store += '-';
				}
			}
		}
		while (!checkOperate(poly[i]))
		{
			store += poly[i];
			i++;
		}
		tmp.coe = atof(store.c_str());

		int count = 0;
		int j = i+1;


		while (poly[j] != '+' && poly[j] != '-') // dem so bien 
		{
			if (poly[j] >= 'a' && poly[j] <= 'z')
			{
				count++;
			}
			j++;
		}

		tmp.sobien = count;
		tmp.var = new char[count];
		tmp.exp = new unsigned short[count];
		count = -1;

		while ( i != j ) 
		{
			if (poly[i] >= 'a' && poly[i] <= 'z') // gap bien 
			{
				++count;
				tmp.var[count] = poly[i]; // lay bien 

				string exp_store = "";

				if (poly[i + 1] == '^') // kiem tra mu 
				{
					i += 2;

					while (poly[i] != '*' && poly[i] != '+' && poly[i] != '-') // lay so mu 
					{
						exp_store += poly[i];
						i++;
					}
				}
				if (exp_store == "")
				{
					tmp.exp[count] = 1;
					i++;
				}
				else
				{
					char *exp_char = new char[exp_store.length() + 1];
					strcpy(exp_char, exp_store.c_str());
					tmp.exp[count] = atoi(exp_char);
				}
				continue;
			}
			i++;
		}
		enqueue(tmp);
		//cout << pHead->pNext->pNext->coe << endl;
	}
}

//enqueue a monomial into polynomial
void Polymial::enqueue(monomial data)
{
	monomial *tmp = createMono(data);
	if (pHead == NULL)
	{
		pHead = pTail = tmp;
	}
	else
	{
		pTail->pNext = tmp;
		pTail = tmp;
		pTail->pNext = NULL;
	}
}

//dequeue a node from polonomial
monomial *Polymial::dequeue()
{
	monomial *tmp = pHead;
	pHead = pHead->pNext;
	return tmp;
}

//check operator
bool Polymial::checkOperate(char ch)
{
	return (ch == '+' || ch == '-' || ch == '*' || ch == '/');
}


int Polymial::compare(monomial T, monomial P)
{
		int n = T.sobien ;
		int m = P.sobien ;

		int temp1 = 0, temp2 = 0;

		for (int i = 0; i < n; i++)
			temp1 += T.exp[i]*(T.var[i]) % BASE ;

		for (int j = 0; j < m; j++)
			temp2 += P.exp[j]*(P.var[j]) % BASE ;

		if ( temp1 == temp2 ) return 0;
		if ( temp1 > temp2 ) return 1 ;
		return -1;
}
//shorten polynomial by deleting node has coe=0
void Polymial::shorten()
{
	if (!pHead) return;

	monomial *tmp = pHead;

	while (tmp)
	{
		if (tmp->coe != 0) // cap nhat he so cho nhung don thuc co bien giong nhau 
		{
			monomial *tmp2 = tmp->pNext;
			while (tmp2)
			{
				if (compare(*tmp, *tmp2) == 0) // Da xu li 3 truong hop (a^2*b , b*a^2 ) va ( a^2*b^2 , b^2*a^2) va ( a^2*b^2 , a^2*b^2)
				{
					tmp->coe += tmp2->coe;
					tmp2->coe = 0;
				}
				tmp2 = tmp2->pNext;
			}
		}
		tmp = tmp->pNext;
	}
	//Delete coe = 0
	tmp = pHead;
	monomial *tmp2 = tmp->pNext;
	if (fabs(tmp->coe) < Min)
	{
		pHead = pHead->pNext;
		delete tmp;
		tmp = pHead;
	}
	while (tmp2 != NULL)
	{
		if (fabs(tmp2->coe) < Min)
		{
			monomial *tmp_del = tmp2;
			tmp2 = tmp2->pNext;
			tmp->pNext = tmp2;
			delete tmp_del;
		}
		else
		{
			tmp = tmp2;
			tmp2 = tmp2->pNext;
		}
	}
}

void Swap(monomial &a, monomial &b)
{
	monomial tmp(a);
	a.sobien = b.sobien;
	a.coe = b.coe;
	a.var = b.var;
	a.exp = b.exp;
	b.coe = tmp.coe;
	b.var = tmp.var;
	b.exp = tmp.exp;
	b.sobien = tmp.sobien;
}
//sorting poly base on exponent and variable(in case exponents equal)
void Polymial::sortPoly()
{
	monomial *tmp = pHead;
	int mu1, mu2, m, n;
	// sap xep tang dan theo mu , neu mu bang nhau thi so sanh bien 
	while (tmp != NULL) // for ( i = 0 ; i < so luong don thuc ; i ++ ) 
	{
		monomial *tmp2 = tmp->pNext;
		m = tmp->sobien; // so luong bien trong don thuc  dang xet 
		mu1 = 0;
		for (int i = 0; i < m; i++)
			mu1 += tmp->exp[i]; // tinh tong mu cua cac bien trong don thuc , A

		while (tmp2 != NULL) // for ( j = i + 1 ; j < so luong don thuc ; j ++ ) 
		{
			n = tmp2->sobien;
			mu2 = 0;
			for (int i = 0; i < n; i++)
				mu2 += tmp2->exp[i]; // tinh tong mu cua B

			if (compare(*tmp, *tmp2) == 1) // A > B 
			{
				Swap(*tmp, *tmp2); // doi cho 2 node trong da thuc 
			}

			tmp2 = tmp2->pNext;
		}
		tmp = tmp->pNext;
	}
}

void Polymial::delete_position(int pos)
{
	if (pos == 1)
	{
		pHead = pHead->pNext;
		return;
	}
	monomial *current = new monomial;
	monomial *previous = new monomial;
	current = pHead;
	for (int i = 1; i<pos; i++)
	{
		previous = current;
		current = current->pNext;
	}
	previous->pNext = current->pNext;
}

//add 2 polynomials
void Polymial::add2Poly(Polymial l1, Polymial l2)
{

	monomial *tmp1 = l1.pHead;
	monomial *tmp2 = l2.pHead;
	while (tmp1)
	{
		enqueue(*tmp1);
		tmp1 = tmp1->pNext;
	}
	while (tmp2)
	{
		enqueue(*tmp2);
		tmp2 = tmp2->pNext;
	}
	shorten();
	sortPoly();
}

//sub 2 polynomials
void Polymial::sub2Poly(Polymial l1, Polymial l2)
{
	monomial *tmp1 = l1.pHead;
	monomial *tmp2 = l2.pHead;
	while (tmp1)
	{
		enqueue(*tmp1);
		tmp1 = tmp1->pNext;
	}
	while (tmp2)
	{
		tmp2->coe = tmp2 ->coe * ( -1.0) ;
		enqueue(*tmp2);
		tmp2 = tmp2->pNext;
	}
	shorten();
	sortPoly();
}

//multiply 2 polynomial
void Polymial::multi2Poly(Polymial l1, Polymial l2)
{
	monomial *tmp1 = l1.pHead;
	monomial *tmp2 = l2.pHead;
	monomial tmp;

	while (tmp1)
	{
		while (tmp2)
		{
			tmp = (*tmp1) * (*tmp2);
			enqueue(tmp);
			tmp2 = tmp2->pNext;
		}
		tmp1 = tmp1->pNext;
	}
	shorten();
	sortPoly();
}